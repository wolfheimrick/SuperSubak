#include "messagelistmodel.h"
#include <QStandardPaths>
#include <QRegExp>
#include <QDebug>

MessageListModel::MessageListModel(QObject* parent) : QAbstractListModel(parent) {
    //messages.push_back({"author", "message0", "attachment", "userid", 0}); // normal
    //messages.push_back({"author", "message1", "file:/Users/newin/Pictures/a3aaeda4e303e416f9bcb2814eaa7701.jpg", "userid", 1}); // image
    //messages.push_back({"author", "message2", "replyAttachment", "userid", 2}); // reply
}

QHash<int, QByteArray> MessageListModel::roleNames() const {
    QHash<int,QByteArray> rez;
    rez[AUTHOR]="author";
    rez[USERID]="_id";
    rez[MESSAGE]="message";
    rez[MERGEDNAMEID]="id_author";
    rez[ATTACHMENT]="attachment";
    rez[TYPE]="attachmentType";
    return rez;
}
int MessageListModel::rowCount(const QModelIndex &) const {
    return messages.size();
}
void MessageListModel::clear() {
    if (messages.size() == 0) {
		return;
	}
    beginRemoveRows(QModelIndex(), 0, messages.size() - 1);
    messages.clear();
    endRemoveRows();
}
QVariant MessageListModel::data(const QModelIndex &index, int role) const {
        if (index.row() < 0 || index.row() >= messages.size()) {
            return QVariant();
        }
        MessageData entry = messages[index.row()];
        if (role == AUTHOR) {
            return QVariant(entry.author);
        } else if (role == MESSAGE) {
            return QVariant(entry.message);
        } else if (role == ATTACHMENT) {
            return QVariant(entry.attachment);
        } else if (role == USERID) {
            return QVariant(entry.userid);
        } else if (role == MERGEDNAMEID) {
            return QVariant(entry.userid + entry.author);
        } else if (role == TYPE) {
            return QVariant(entry.type);
        }
        return QVariant();
}

void MessageListModel::markdown2RichText(QString& str) {
    str.replace(QRegExp("((http|https|ftp)\\:\\/\\/\\S+\\.\\S+)"), "<style> a:link {color:'#0085b7';} </style> <a href=\"\\1\">\\1</a> </font>");
    str.replace(QRegExp("\\*(\\S+)\\*"), "<b>\\1</b>");
    str.replace(QRegExp("\\_(\\S+)\\_"), "<i>\\1</i>");
    str.replace(QRegExp("(@\\S+)"), "<font style=\"background:'#a52323';color:'#c46969'\">\\1</font>");
    str.replace(QRegExp("\\`(.*)\\`"), "<font color=\"cyan\"><code>\\1</code></font>");
}

void MessageListModel::sanitiseReply(QString& str) {
    str.replace(QRegExp("\\[ \\]\\(\\S+\\)"), "");
}

void MessageListModel::addMessage(QString, QString message, QString attachment, QString authorName, QString userid, int attachmentType, bool reverseMode) {
    unsigned int index = 0;
    if (reverseMode) {
        qDebug() << "+++++ usingNonReverse";
    } else {
        //index = messages.size() > 0 ? messages.size() - 1 : 0;
        index = messages.size();
        qDebug() << "+++++ usingReverse";
    }

    //QString attachment("loltachement");
    MessageData m{ authorName, message, attachment, userid, attachmentType };
    beginInsertRows(QModelIndex(), index, index);
    if (attachmentType == 1) {
        //m.attachment = "file:/" + (QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/SuperSubakCache/" + attachment);
        //m.message = "attachment: <style> a:link {color:'#0085b7';} </style> <a href=\"file:" + link + "\">" + message + "</a><img src=\"file:" + link + "\" alt=\"attachment\" width=\"200\"/>";
        qDebug() << "imageURL: " << m.attachment;
    } else if (attachmentType == 2) {
        sanitiseReply(message);
        m.message = message;
        markdown2RichText(m.message);
    } else {
        m.message = message;
        markdown2RichText(m.message);
    }
    qDebug() << "adding row message: " << message << authorName;
    messages.insert(index, m);
    endInsertRows();
}
